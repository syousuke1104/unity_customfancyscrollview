﻿using UnityEngine;

namespace FancyScrollViewCustom {

	public class CustomItemData {

		public readonly Color CardColor;
		public readonly string ItemName;

		public CustomItemData(string name, Color color) {
			ItemName = name;
			CardColor = color;
		}

	}

}