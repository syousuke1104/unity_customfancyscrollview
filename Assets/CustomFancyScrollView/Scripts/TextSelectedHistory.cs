﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using TMPro;

namespace FancyScrollViewCustom {

	public class TextSelectedHistory : MonoBehaviour {

		[SerializeField] private CustomFancyScrollView m_scrollView = default;

		private void Awake() {
			TextMeshProUGUI textMesh = transform.GetComponent<TextMeshProUGUI>();
			Queue<string> history = new Queue<string>();
			StringBuilder stringBuilder = new StringBuilder();

			textMesh.text = "";

			m_scrollView.OnUpdateSelection += item => {
				history.Enqueue(item.ItemName);

				if (history.Count > 5) {
					history.Dequeue();
				}

				stringBuilder.Clear();
				foreach (string itemName in history) {
					stringBuilder.AppendLine(itemName);
				}
				textMesh.text = stringBuilder.ToString();

			};
		}

	}
}
