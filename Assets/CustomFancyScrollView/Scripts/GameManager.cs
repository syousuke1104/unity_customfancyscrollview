﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace FancyScrollViewCustom {

	public class GameManager : MonoBehaviour {

		[SerializeField] private CustomFancyScrollView m_scrollView = default;

		private void Start() {

			var items = Enumerable.Range(0, 20)
				.Select(i => new CustomItemData($"Item:{i}", Color.HSVToRGB(1f / 20f * i, 1f, 1f)))
				.ToArray();

			m_scrollView?.UpdateData(items);
		}
	}
}
