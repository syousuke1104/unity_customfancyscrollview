﻿using UnityEngine.Events;

namespace FancyScrollViewCustom {

	public class CustomContext {

		public int SelectedIndex = -1;

		public UnityAction<int> OnCellClicked = delegate { };

	}
}
