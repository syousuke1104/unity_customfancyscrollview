﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace FancyScrollViewCustom {

	public class TextCurrentItemName : MonoBehaviour {

		[SerializeField] private CustomFancyScrollView m_customFancyScrollView = default;

		private void Awake() {
			TextMeshProUGUI textMesh = transform.GetComponent<TextMeshProUGUI>();
			m_customFancyScrollView.OnUpdateCurrentData += item => textMesh.text = item.ItemName;
		}
	}
}
