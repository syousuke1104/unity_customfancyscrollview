﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using FancyScrollView;

namespace FancyScrollViewCustom {

	[RequireComponent(typeof(Scroller))]
	public class CustomFancyScrollView : FancyScrollView<CustomItemData, CustomContext> {

		[SerializeField] private GameObject m_prefabCell = default;

		public event UnityAction<CustomItemData> OnUpdateCurrentData = delegate { };
		public event UnityAction<CustomItemData> OnUpdateSelection = delegate { };

		private Scroller m_scroller;

		protected override GameObject CellPrefab => m_prefabCell;

		private void Awake() {
			m_scroller = transform.GetComponent<Scroller>();

			// スクロール中の処理を登録
			m_scroller.OnValueChanged(position => {
				UpdatePosition(position); // 必須

				int index = Mathf.Clamp(Mathf.RoundToInt(Mathf.Repeat(position, ItemsSource.Count)), 0, ItemsSource.Count - 1);
				OnUpdateCurrentData(ItemsSource[index]);
			});
			// 選択されるセルが決まったときの処理を登録
			m_scroller.OnSelectionChanged(UpdateSelection);

			// セルをクリックしたときの処理を登録
			Context.OnCellClicked += ScollAndSelectCell;
		}

		private void Start() {
			OnUpdateCurrentData(ItemsSource[0]);
		}

		// スクロールビューに表示できるデータを更新
		public void UpdateData(IList<CustomItemData> items) {
			UpdateContents(items);
			m_scroller.SetTotalCount(items.Count);
		}

		// 選択されるセルが決まったときに行う
		private void UpdateSelection(int index) {

			if (Context.SelectedIndex == index) return;

			// コンテキストの値を更新
			Context.SelectedIndex = index;
			// とりあえず更新しておく
			Refresh();

			OnUpdateSelection(ItemsSource[index]);
		}

		// スクロールとセルの選択
		private void ScollAndSelectCell(int index) {

			if (index < 0 || index >= ItemsSource.Count) {
				Debug.LogError("インデックス番号が範囲外です");
				return;
			}

			if (Context.SelectedIndex == index) return;

			UpdateSelection(index);
			m_scroller.ScrollTo(index, 0.35f, EasingCore.Ease.OutCubic);
		}
	}
}
