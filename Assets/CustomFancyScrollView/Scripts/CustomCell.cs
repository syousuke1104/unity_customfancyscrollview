﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using FancyScrollView;

namespace FancyScrollViewCustom {

	[RequireComponent(typeof(CanvasGroup))]
	public class CustomCell : FancyScrollViewCell<CustomItemData, CustomContext> {

		private CanvasGroup m_canvasGroup;
		private Button m_button;
		private TextMeshProUGUI m_text;
		private RectTransform m_childRectTransform;

		private void Awake() {
			m_canvasGroup = transform.GetComponent<CanvasGroup>();

			m_button = transform.GetComponentInChildren<Button>();
			m_text = transform.GetComponentInChildren<TextMeshProUGUI>();
			m_childRectTransform = transform.GetChild(0).GetComponent<RectTransform>();

			// ボタンを押したとき、コンテキストを通じてスクロールビューに通知
			// スクロールビュー側ではセルの選択を行う
			m_button.onClick.AddListener(() => Context.OnCellClicked(Index));
		}

		public override void UpdateContent(CustomItemData itemData) {
			// セルの中に表示する内容を更新
			m_text.text = itemData.ItemName;
			m_button.image.color = itemData.CardColor;
		}

		public override void UpdatePosition(float position) {
			// セルの位置によって見え方を更新 ---- position = 0 ~ 1.0

			// 透明度
			m_canvasGroup.alpha = Mathf.Sin(position * Mathf.PI);

			// ローカル座標
			float radius = m_childRectTransform.sizeDelta.x;
			float x = -Mathf.Cos(position * Mathf.PI) * radius;
			float z = -Mathf.Sin(position * Mathf.PI) * radius + radius;
			m_childRectTransform.localPosition = new Vector3(x, 0, z);

			// ローカル回転値
			float angle = (1f - position * 2f) * 90;
			m_childRectTransform.localRotation = Quaternion.Euler(0, angle, 0);
		}
	}
}
